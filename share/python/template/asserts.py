# Check two values for equality and give error if they are not equal
def assert_eq(a, b):
    assert a == b, "expected %r == %r" % (a, b)


# Check two values for inequality and give error if they are equal
def assert_ne(a, b):
    assert a != b, "expected %r != %r" % (a, b)


# Check that two dict values are equal.
def assert_dict_eq(a, b):
    assert isinstance(a, dict)
    assert isinstance(b, dict)
    for key in a:
        assert key in b, f"exected {key} in both dicts"
        av = a[key]
        bv = b[key]
        assert_eq(type(av), type(bv))
        if isinstance(av, list):
            assert_eq(list(sorted(av)), list(sorted(bv)))
    for key in b:
        assert key in a, f"exected {key} in both dicts"
