#!/bin/bash

# Store files embedded in the markdown input.

files_new() {
    dict_new _files
}

files_set() {
    dict_set _files "$1" "$2"
}

files_get() {
    dict_get _files "$1"
}


# Decode a base64 encoded string.

decode_base64() {
    echo "$1" | base64 -d
}
