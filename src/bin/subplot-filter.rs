use anyhow::Result;
use std::io::{self, Read, Write};
use subplot::{Document, Style};

fn main() -> Result<()> {
    let mut buffer = String::new();
    let mut stdin = io::stdin();
    stdin.read_to_string(&mut buffer)?;
    let basedir = std::path::Path::new(".");
    let style = Style::default();
    let mut doc = Document::from_json(&basedir, vec![], &buffer, style)?;
    doc.typeset();
    let bytes = doc.ast()?.into_bytes();
    io::stdout().write_all(&bytes)?;
    Ok(())
}
