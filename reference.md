# Introduction

This document describes how we guard against accidental breaking
changes in Subplot by running it against a curated set of subplot
documents.

# Subplot

## Produce a PDF

~~~scenario
given an installed subplot
given a clone of https://gitlab.com/subplot/subplot.git in src at bfbcaf9eedb9f2339494c3ee16d5761121631342
when I docgen subplot.md to test.pdf, in src
then file src/test.pdf exists
~~~

## Produce HTML page

~~~scenario
given an installed subplot
given a clone of https://gitlab.com/subplot/subplot.git in src at bfbcaf9eedb9f2339494c3ee16d5761121631342
when I docgen subplot.md to test.html, in src
when I run, in src, subplot docgen subplot.md -o subplot.html
then file src/test.html exists
~~~

## Generate and run test program

~~~scenario
given an installed subplot
given file run_test.sh
given a clone of https://gitlab.com/subplot/subplot.git in src at bfbcaf9eedb9f2339494c3ee16d5761121631342
when I run, in src, subplot codegen subplot.md -o test-inner.py
when I run bash run_test.sh
then command is successful
~~~

~~~{#run_test.sh .file .sh}
#!/bin/bash

set -euo pipefail
python3 src/test-inner.py --log test-inner.log --env "PATH=$PATH" --env SUBPLOT_DIR=/
~~~


---
title: Test Subplot against reference subplots
author: The Subplot project
template: python
bindings:
- reference.yaml
- subplot.yaml
- lib/runcmd.yaml
- lib/files.yaml
functions:
- reference.py
- subplot.py
- lib/files.py
- lib/runcmd.py
...
